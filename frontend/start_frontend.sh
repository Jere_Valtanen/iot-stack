#!/bin/bash

sudo mkdir -p ./data/grafana

# Start all docker containers.
echo "Starting up all containers!"
sudo docker-compose -f docker-compose-frontend.yml up -d