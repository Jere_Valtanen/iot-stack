#!/bin/bash

# Define environment variables
export PROD_BROKER=ssl://rabbitmq:8883

# Start producer.
echo "Starting up producer!"
sudo docker-compose -f docker-compose-producer.yml up -d