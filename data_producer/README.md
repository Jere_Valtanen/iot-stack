Created by Jere-Joonas Valtanen

This is a Java Spring boot application
It fetches worldwide air pollution data from the openaq API

To install: sudo mvn clean install
To run jar: java -jar target/data_producer-0.0.1-SNAPSHOT.jar

Requires ssh certificates to be generated for rabbitmq connection