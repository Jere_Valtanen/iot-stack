package com.iot_stack.data_producer.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DateInfo {
    private Date utc;
    private Date local;

    public DateInfo() {
    }

    /**
     * @return the local
     */
    public Date getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(Date local) {
        this.local = local;
    }

    /**
     * @return the utc
     */
    public Date getUtc() {
        return utc;
    }

    /**
     * @param utc the utc to set
     */
    public void setUtc(Date utc) {
        this.utc = utc;
    }

}