package com.iot_stack.data_producer.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pollution {

    private Results[] results;

    public Pollution() {
    }

    /**
     * @return the results
     */
    public Results[] getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(Results[] results) {
        this.results = results;
    }


    public String printResults(){
        StringBuilder s = new StringBuilder();
        for (Results result : results) {
            s.append(result + ", \n");
        }
        return s.toString();
    }

    @Override
    public String toString() {
        return "Data{" +
            "Results:" + printResults() +        
            '}';
    }

}