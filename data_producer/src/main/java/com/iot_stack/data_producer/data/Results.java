package com.iot_stack.data_producer.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {
    private String location;
    private String parameter;
    @JsonProperty("date")
    private DateInfo dateInfo;
    private Float value;
    private String unit;
    private Coordinates coordinates;
    private String country;
    private String city;


    public Results() {
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

        /**
     * @return the dateInfo
     */
    public DateInfo getDateInfo() {
        return dateInfo;
    }

    /**
     * @param dateInfo the dateInfo to set
     */
    public void setDateInfo(DateInfo dateInfo) {
        this.dateInfo = dateInfo;
    }

    /**
     * @return the dateInfo
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * @param coordinates the coordinates to set
     */
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * @return the parameter
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * @param parameter the parameter to set
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Weather{" +
            "Location='" + location + '\'' +
            "Parameter='" + parameter + '\'' +
            "Date='" + dateInfo.getUtc().getTime() + '\'' +
            "Value='" + value + '\'' +
            "Unit='" + unit + '\'' +
            "Coordinates='" + coordinates.toString() + '\'' +
            "Country='" + country + '\'' +      
            "City='" + city + '\'' +        
            '}';
    }
}