package com.iot_stack.data_producer.rabbitmq;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ResourceBundle;

public class SslSecurity {
    private static final Logger log = LoggerFactory.getLogger(SslSecurity.class);

    private static final String PROPERTIES_FILENAME = "DATA_PRODUCER";
    private static ResourceBundle propertiesBundle;

    private static String caFilePath;
    private static String clientCrtFilePath;
    private static String clientKeyFilePath;

    static {
        propertiesBundle = ResourceBundle.getBundle(PROPERTIES_FILENAME);

        caFilePath = propertiesBundle.getString("prod_data.cafilepath");

        if (System.getenv("PRODUCER_MQTT_CA_FILEPATH") != null) {
            caFilePath = System.getenv("PRODUCER_MQTT_CA_FILEPATH");
        }

        clientCrtFilePath = propertiesBundle.getString("prod_data.clientcrtfilepath");

        if (System.getenv("PRODUCER_MQTT_CLIENT_CRT_FILEPATH") != null) {
            clientCrtFilePath = System.getenv("PRODUCER_MQTT_CLIENT_CRT_FILEPATH");
        }

        clientKeyFilePath = propertiesBundle.getString("prod_data.clientkeyfilepath");

        if (System.getenv("PRODUCER_MQTT_CLIENT_KEY_FILEPATH") != null) {
            clientKeyFilePath = System.getenv("PRODUCER_MQTT_CLIENT_KEY_FILEPATH");
        }
    }

    public static SSLSocketFactory getSslSocketFactory(final String keyFilePassword) {
        try {
            return SslSecurity.getSocketFactory(caFilePath,
                    clientCrtFilePath, clientKeyFilePath, keyFilePassword);
        } catch (Exception e) {
            log.error("Exception while generating SSL socket factory. ", e);
        }
        return null;
    }

    public static SSLSocketFactory getSslSocketFactory() {
        try {
            return SslSecurity.getSocketFactory(caFilePath,
                    clientCrtFilePath, clientKeyFilePath, "");
        } catch (Exception e) {
            log.error("Exception while generating SSL socket factory. ", e);
        }
        return null;
    }

    private static X509Certificate getCertificateFromFile(final String file)
            throws IOException, CertificateException {
		X509Certificate cert = null;
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		while (bis.available() > 0) {
		    cert = (X509Certificate) cf.generateCertificate(bis);
		}

        bis.close();
        return cert;
    }

    private static KeyPair getKeyPairFromFile(final String file, final String password)
            throws IOException {
        KeyPair key = null;

		PEMParser pemParser = new PEMParser(new FileReader(file));
        Object object = pemParser.readObject();

        PEMDecryptorProvider decProv =
            new JcePEMDecryptorProviderBuilder()
                .build(password.toCharArray());
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");

		if (object instanceof PEMEncryptedKeyPair) {
			log.info("Encrypted key - we will use provided password");
			key = converter.getKeyPair(((PEMEncryptedKeyPair) object)
					.decryptKeyPair(decProv));
		} else {
			log.info("Unencrypted key - no password needed");
			key = converter.getKeyPair((PEMKeyPair) object);
        }

        pemParser.close();
        return key;
    }

    private static SSLSocketFactory getSocketFactory(final String caCrtFile, final String crtFile,
            final String keyFile, final String keyFilePassword)
            throws IOException, CertificateException,
            KeyStoreException, NoSuchAlgorithmException,
            UnrecoverableKeyException, KeyManagementException {
		Security.addProvider(new BouncyCastleProvider());

		// load CA certificate
		X509Certificate caCert = getCertificateFromFile(caCrtFile);

		// load client certificate
        X509Certificate cert = getCertificateFromFile(crtFile);

		// load client private key
        KeyPair key = getKeyPairFromFile(keyFile, keyFilePassword);

		// CA certificate is used to authenticate server
		KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
		caKs.load(null, null);
        caKs.setCertificateEntry("ca-certificate", caCert);

		TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
		tmf.init(caKs);

		// client key and certificates are sent to server so it can authenticate us
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());

		ks.load(null, null);
        ks.setCertificateEntry("certificate", cert);

        ks.setKeyEntry("private-key", key.getPrivate(),
            keyFilePassword.toCharArray(),
                new java.security.cert.Certificate[] { cert });

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(
            KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, keyFilePassword.toCharArray());

		// finally, create SSL socket factory
		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		return context.getSocketFactory();
	}
}