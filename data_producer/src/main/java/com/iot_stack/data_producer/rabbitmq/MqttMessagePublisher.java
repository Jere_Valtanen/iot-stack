package com.iot_stack.data_producer.rabbitmq;

import java.util.ResourceBundle;

import javax.net.ssl.SSLSocketFactory;

import com.iot_stack.data_producer.data.Results;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MqttMessagePublisher {
    private static final Logger log = LoggerFactory.getLogger(MqttMessagePublisher.class);

    private static final String PROPERTIES_FILENAME = "DATA_PRODUCER";

    private static ResourceBundle propertiesBundle;

    private MqttClient client;

    private static String broker;
    private static String clientId;

    private static String username;
    private static String password;

    static {
        propertiesBundle = ResourceBundle.getBundle(PROPERTIES_FILENAME);

        
        broker = propertiesBundle.getString("prod_data.broker");

        if (System.getenv("PROD_BROKER") != null) {
            broker = System.getenv("PROD_BROKER");
        }

        clientId = propertiesBundle.getString("prod_data.clientid");

        username = propertiesBundle.getString("prod_data.mqttusername");

        password = propertiesBundle.getString("prod_data.mqttpassword");
    }

    public MqttMessagePublisher() { }

    public void connect() throws MqttException {
        log.info("Connecting to Mqtt broker {} with client {}", broker, clientId);

        client = new MqttClient(broker, clientId, new MemoryPersistence());

        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        connOpts.setUserName(username);
        connOpts.setPassword(password.toCharArray());

        if (broker != null && broker.substring(0, 3).equalsIgnoreCase("SSL")) {
            SSLSocketFactory socketFactory = SslSecurity.getSslSocketFactory();
            connOpts.setSocketFactory(socketFactory);
        }

        client.connect(connOpts);
    }

    private void publish(String data, String country, String city, String type) throws MqttException {
        String topic = country + "/" + city + "/" + type;
        log.info("Publishing message {} on topic {}", data, topic);

        MqttMessage message = new MqttMessage(data.getBytes());
        message.setQos(1);
        client.publish(topic, message);
    }

    public void publishMessage(Results result, String country, String city, String type) {
        log.info("Received an array of IoT data objects; publishing messages.");
        try
        {
            JSONObject message = MessageParser.generateIotDataMessage(result);

        log.info("Publishing message {}", message);
        this.publish(message.toString(), country, city, type);
        }
        catch(Exception e)
        {
            log.error(e.toString());
        }
    }

    public void disconnect() throws MqttException {
        client.disconnect();
    }
}