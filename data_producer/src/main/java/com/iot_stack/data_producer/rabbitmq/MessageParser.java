package com.iot_stack.data_producer.rabbitmq;

import com.iot_stack.data_producer.data.Results;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MessageParser {

    /*
     * Generate JSONObjects from IotData object.
     *
     */
    
    static JSONObject generateIotDataMessage(Results result) {

        JSONObject meta = new JSONObject();
        JSONArray tags = new JSONArray();
        JSONObject res = new JSONObject();
        JSONObject measurement = new JSONObject();

        measurement.put("Value", result.getValue());
        measurement.put("Unit", result.getUnit());
        measurement.put("Latitude", result.getCoordinates().getLatitude());
        measurement.put("Longitude", result.getCoordinates().getLongitude());

        JSONObject id = new JSONObject();
        id.put("id",
            result.getLocation());
        tags.add(id);

        JSONObject param = new JSONObject();
        param.put("parameter",
            result.getParameter());
        tags.add(param);

        JSONObject country = new JSONObject();
        country.put("country",
            result.getCountry());
        tags.add(country);

        JSONObject city = new JSONObject();
        city.put("city",
            result.getCity());
        tags.add(city);

        meta.put("tags", tags);

        res.put("type", "AirQualityData");
        res.put("uuid", result.getLocation());
        res.put("utc", result.getDateInfo().getUtc().getTime());
        res.put("meta", meta.toJSONString());
        res.put("data", measurement.toJSONString());

        return res;
    }
}