package com.iot_stack.data_producer;

import com.iot_stack.data_producer.data.Pollution;
import com.iot_stack.data_producer.data.Results;
import com.iot_stack.data_producer.rabbitmq.MqttMessagePublisher;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

/*
This is the "main" class that periodically fetches data from the REST api,
and publishes it to rabbitmq
*/
@Component
public class DataFetcher {

    MqttMessagePublisher mqttMessagePublisher = new MqttMessagePublisher();

    private static final Logger log = LoggerFactory.getLogger(DataFetcher.class);
    
    private final RestTemplate restTemplate;

    public DataFetcher(RestTemplateBuilder restTemplateBuilder) {
        restTemplateBuilder.setConnectTimeout(Duration.ofMillis(10));
		this.restTemplate = restTemplateBuilder.build();
	}

    // Fetch new data every 15 minutes
    @Scheduled(fixedRate = 900000)
    public void scheduledTasks() throws InterruptedException {
        fetchData();
    }

    public void fetchData() {
        log.info("Fetching air pollution data");
        
        Pollution weather = restTemplate.getForObject(
					"https://api.openaq.org/v1/measurements?limit=10000", Pollution.class);
        
        log.info("Data fetched");
        
        // Establish connection to rabbitmq
        try {
            mqttMessagePublisher.connect();
        } catch (MqttException e) {
            log.error(e.getMessage());
        }

        // Send publish results to rabbitmq
        for (Results result : weather.getResults()) {
            try {
                mqttMessagePublisher.publishMessage(result, result.getCountry(), result.getCity(), result.getParameter());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }
}
