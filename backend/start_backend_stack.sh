#!/bin/bash

# Define environment variables
export QUEUE_USERNAME=root
export QUEUE_PASSWORD=root
export QUEUE_NAME=source

# Create data directories for container volumes.
# This way important data is kept even if docker container fails
# i.e influxdb database
sudo mkdir -p /data/influxdb
sudo mkdir -p /data/rabbitmq

# Start all docker containers.
echo "Starting up all containers!"
sudo docker-compose -f docker-compose-backend.yml up -d

# Wait for rabbitmq to startup. 10 seconds is plenty
echo -n -e "Waiting for rabbitmq to finish starting up\n"
sleep 10

./create_rabbitmq_queues.sh