Created by Jere-Joonas Valtanen

The backend contains rabbitmq, data consumer and influxdb

The data consumer jar must be built before running the startup script
build consumer: sudo mvn clean install
run consumer outside docker: java -jar -Dserver.port=8081 target/data_consumer-1.0.0.jar

build and run backend docker containers: run start_backend_stack.sh script