#!/bin/bash

# Define environment variables
export QUEUE_USERNAME=root
export QUEUE_PASSWORD=root
export QUEUE_NAME=source

# Let's use the rabbitmqadmin tool to create an incoming queue for messages Create incoming queue for messages and bind(forward) incoming MQTT to that queue.
echo -n "Creating RabbitMQ message queue co: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare queue name=co`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating RabbitMQ message queue pm25: "
message=`./rabbitmqadmin --host=localhost --port=15672 \
--username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD \
declare queue name=pm25`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating RabbitMQ message queue pm10: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare queue name=pm10`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating RabbitMQ message queue no2: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare queue name=no2`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating RabbitMQ message queue o3: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare queue name=o3`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating RabbitMQ message queue so2: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare queue name=so2`

if [[ $message == *"queue declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi





# Finally we bind incoming MQTT messages to that queue.
echo -n "Creating binding[MQTT->co]: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare binding source="amq.topic" destination=co routing_key="*.*.co"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating binding[MQTT->pm25]: "
message=`./rabbitmqadmin --host=localhost --port=15672 \
--username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD \
declare binding source="amq.topic" destination=pm25 routing_key="*.*.pm25"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating binding[MQTT->pm10]: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare binding source="amq.topic" destination=pm10 routing_key="*.*.pm10"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating binding[MQTT->no2]: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare binding source="amq.topic" destination=no2 routing_key="*.*.no2"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating binding[MQTT->o3]: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare binding source="amq.topic" destination=o3 routing_key="*.*.o3"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi

echo -n "Creating binding[MQTT->so2]: "
message=`./rabbitmqadmin --host=localhost --port=15672 --username=$QUEUE_USERNAME --password=$QUEUE_PASSWORD declare binding source="amq.topic" destination=so2 routing_key="*.*.so2"`
if [[ $message == *"binding declared"* ]]; then
	echo "[OK]"
else
	echo "$message [ERROR]"
fi