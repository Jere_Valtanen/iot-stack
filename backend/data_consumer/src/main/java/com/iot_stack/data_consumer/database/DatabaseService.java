package com.iot_stack.data_consumer.database;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.Map;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import com.iot_stack.data_consumer.dto.IotData;
import com.iot_stack.data_consumer.ApplicationException;

@Service
public class DatabaseService {

    private static final Logger log = LoggerFactory.getLogger(DatabaseService.class);

    private final static String PROPERTIES_FILENAME = "CONSUMERAPP";
    private static ResourceBundle propertiesBundle;

    private InfluxDB influxDB;

    private static String dbHost;
    private static String dbName;
    private static String dbUser;
    private static String dbPassword;
    private static long dbConnRefreshInterval;

    private final static String DB_CONNECT_HTTP_PROTOCOL = "http://";


    static {
        propertiesBundle = ResourceBundle.getBundle(PROPERTIES_FILENAME);

        dbHost = propertiesBundle.getString("consumerapp.databasehost");

        if (System.getenv("DATABASE_HOST") != null) {
            dbHost = System.getenv("DATABASE_HOST");
        }

        dbHost = DB_CONNECT_HTTP_PROTOCOL + dbHost;

        dbName = propertiesBundle.getString("consumerapp.databasename");

        if (System.getenv("DATABASE") != null) {
            dbName = System.getenv("DATABASE");
        }

        dbUser = propertiesBundle.getString("consumerapp.databaseuser");

        if (System.getenv("DATABASE_USERNAME") != null) {
            dbUser = System.getenv("DATABASE_USERNAME");
        }

        dbPassword = propertiesBundle.getString("consumerapp.databasepasswd");

        if (System.getenv("DATABASE_PASSWORD") != null) {
            dbPassword = System.getenv("DATABASE_PASSWORD");
        }

        dbConnRefreshInterval = Long.parseLong(
            propertiesBundle.getString("consumerapp.dbconnrefreshminutes"));
    }

    public DatabaseService() { }

    public static long getDbConnRefreshIntervalInMillis() {
        return dbConnRefreshInterval * 1000 * 60;
    }

    public void connect() {
        if (influxDB == null) {
            log.info("Connecting to Influx DB {} with {}", dbHost, dbUser);
            influxDB = InfluxDBFactory.connect(dbHost, dbUser, dbPassword);
            
            // If the database named target doesn't yet exist, create it
            if(!influxDB.databaseExists("target"))
                influxDB.createDatabase("target");
                influxDB.setRetentionPolicy(
                    "autogen");
            return;
        }
        log.info("Connection already open {}", dbHost);
    }

    public void close() {
        log.info("Closing the InfluxDB connection.");

        if (influxDB != null) {
            try {
                influxDB.close();
                influxDB = null;
            } catch (Exception e) {
                influxDB = null;
            }
        }
     }

    private Map<String, String> getMetaTagsForPoint(final JSONObject meta) throws JSONException {
        Map<String, String> tags = new HashMap<String, String>();

        JSONArray taglist = meta.getJSONArray("tags");

        for (int i = 0; i < taglist.length(); i++) {
            JSONObject tag = taglist.getJSONObject(i);

            Iterator<String> keys = tag.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                String val = (String) tag.get(key);
                tags.put(key, val);
            }
        }
        return tags;
    }

    private Point pointBuilder(final IotData data) throws ApplicationException {

        try {
            String type = data.getType();
            String id = data.getUuid();
            long utc = data.getUtc();

            log.info("Generating Influx DB Point type: {} uuid: {}", type, id);

            JSONObject obj = new JSONObject(data.getData());
            Map<String, Object> fields = new HashMap<String, Object>();
            fields.put("uuid", id);

            Iterator<String> keys = obj.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                Object o = obj.get(key);
                fields.put(key, o);
            }

            JSONObject meta = new JSONObject(data.getMeta());
            Map<String, String> tags = this.getMetaTagsForPoint(meta);

            Point p = Point.measurement(type)
                .time(utc, TimeUnit.MILLISECONDS)
                .tag(tags)
                .fields(fields).build();

            return p;

        } catch (JSONException e) {
            log.error("Caught JSONException while parsing IOT data.");
            throw new ApplicationException(e);
        }
    }

    public void write(final IotData data) throws ApplicationException {
        log.info("Writing data into Influx DB {}", data.toString());

        Point p = this.pointBuilder(data);
        influxDB.write(dbName, "autogen", p);
     }
}
