package com.iot_stack.data_consumer;

public class ApplicationException extends Exception {
    static final long serialVersionUID = 1L;

    public ApplicationException(String s) {
        super(s);
    }

    public ApplicationException(Exception e) {
        super(e);
    }
}
