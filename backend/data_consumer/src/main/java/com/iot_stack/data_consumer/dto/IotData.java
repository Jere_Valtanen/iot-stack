package com.iot_stack.data_consumer.dto;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IotData {

	private String uuid;
	private String type;
	private long utc;

	private String data;
	private String meta;

	@JsonCreator
	public static IotData createfromJson(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		IotData data = mapper.readValue(json, IotData.class);
		return data;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getUtc() {
		return this.utc;
	}

	public void setUtc(long utc) {
		this.utc = utc;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	@Override
	public String toString() {
		return "IotData [uuid=" + uuid + ", type=" + type + ", utc=" + utc + ", data=" + data + ", meta=" + meta + "]";
	}
}