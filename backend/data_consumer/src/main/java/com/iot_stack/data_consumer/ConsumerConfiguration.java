package com.iot_stack.data_consumer;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

import com.iot_stack.data_consumer.rabbitmq.MessageConsumer;

@Configuration
public class ConsumerConfiguration {
    private static final Logger log = LoggerFactory.getLogger(ConsumerConfiguration.class);

    public static String INCOMING_QUEUE;
    public static String DEAD_LETTER_QUEUE;

    private final static String PROPERTIES_FILENAME = "CONSUMERAPP";
    private static ResourceBundle propertiesBundle;

    static {
       propertiesBundle = ResourceBundle.getBundle(PROPERTIES_FILENAME);

       INCOMING_QUEUE = getQueueName();
       DEAD_LETTER_QUEUE = INCOMING_QUEUE + ".dlx";

    }

    private static String getQueueName() {
        String name = propertiesBundle.getString("consumerapp.rabbitmqueuename");

        if (System.getenv("QUEUE_NAME") != null) {
            name = System.getenv("QUEUE_NAME");
        }
        return name;
    }

    private int getNumberOfConsumers() {
        int concurrentConsumers = 1;

        if (System.getenv("NO_OF_CONSUMERS") != null) {
			concurrentConsumers = Integer.parseInt(
                System.getenv("NO_OF_CONSUMERS"));
		} else {
            concurrentConsumers = Integer.parseInt(
                propertiesBundle.getString("consumerapp.numofconsumers"));
        }
        return concurrentConsumers;
    }

    private int getMaxConcurrentConsumers() {
        int maxConcurrentConsumers = 1;

        if (System.getenv("MAX_CONSUMERS") != null) {
			maxConcurrentConsumers = Integer.parseInt(
                System.getenv("MAX_CONSUMERS"));
		} else {
            maxConcurrentConsumers = Integer.parseInt(
                propertiesBundle.getString("consumerapp.maxconsumers"));
        }
        return maxConcurrentConsumers;
    }

    // TODO we should configure dead-letter-exchange for retrying failed messages?

    // @Bean
    // Queue incomingQueue() {
    //     return QueueBuilder.durable(INCOMING_QUEUE)
    //             .withArgument("x-dead-letter-exchange", "")
    //             .withArgument("x-dead-letter-routing-key", DEAD_LETTER_QUEUE)
    //             .build();
    // }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setRoutingKey("#");
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        String hostName = propertiesBundle.getString("consumerapp.rabbitmqhost");

        if (System.getenv("RABBITMQ_HOST") != null) {
			hostName = System.getenv("RABBITMQ_HOST");
		}

        String username = propertiesBundle.getString("consumerapp.rabbitmquser");

        if (System.getenv("QUEUE_USERNAME") != null) {
			username = System.getenv("QUEUE_USERNAME");
		}

        String password = propertiesBundle.getString("consumerapp.rabbitmqpasswd");

        if (System.getenv("QUEUE_PASSWORD") != null) {
			password = System.getenv("QUEUE_PASSWORD");
		}

        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(hostName);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);

        return connectionFactory;
    }

    @Bean
    public SimpleMessageListenerContainer container() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        ConnectionFactory connectionFactory = connectionFactory();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames("co", "pm25", "o3", "no2", "so2", "pm10");
        container.setMessageListener(getMessageListener());
        container.setMessageConverter(jsonMessageConverter());
        container.setConcurrentConsumers(getNumberOfConsumers());
        container.setMaxConcurrentConsumers(getMaxConcurrentConsumers());

        log.info("Hostname {}", connectionFactory.getHost());
        log.info("Port {}", connectionFactory.getPort());

        return container;
    }

    @Bean
    public MessageListener getMessageListener() {
        return new MessageConsumer();
    }

}
