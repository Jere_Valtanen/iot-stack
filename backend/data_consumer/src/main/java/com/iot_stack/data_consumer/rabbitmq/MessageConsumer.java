package com.iot_stack.data_consumer.rabbitmq;

import java.util.concurrent.CountDownLatch;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iot_stack.data_consumer.database.DatabaseService;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import com.iot_stack.data_consumer.dto.IotData;
import com.iot_stack.data_consumer.ApplicationException;

@Service
public class MessageConsumer implements MessageListener {
    private static final Logger log = LoggerFactory.getLogger(MessageConsumer.class);

    private CountDownLatch latch = new CountDownLatch(1);

    private long lastRefreshTime = System.currentTimeMillis();
    private static long dbRefreshTimeInterval;

    static {
        dbRefreshTimeInterval =
            DatabaseService.getDbConnRefreshIntervalInMillis();

        if (dbRefreshTimeInterval <= 0) {
            // set 5 min default
            dbRefreshTimeInterval = 15 * 60 * 1000;
        }
    }

    @Autowired
    DatabaseService db;

    private void messageToDeadLetterExchange(String message, String error) {
        log.error("Sending message to DLX due exceptions {}", message);
        throw new AmqpRejectAndDontRequeueException(error);
    }

    @Override
    public void onMessage(Message message) {
        String jsonBody = new String(message.getBody());
        log.info("Consuming data {}", jsonBody);

        // We received a message from the RabbitMq store queue
        final long now = System.currentTimeMillis();

        try {
            if (lastRefreshTime + dbRefreshTimeInterval < now) {
                // Time passed since the last message; refresh the DB connection
                lastRefreshTime = now;
                db.close();
            }
            db.connect();
            IotData data = IotData.createfromJson(jsonBody);
            db.write(data);



        } catch (ApplicationException e) {
            log.error("Caught Application Exception when handling data.", e);
            messageToDeadLetterExchange(jsonBody, e.getMessage());
        } catch (Exception e) {
            db.close();
            log.error("Unexpected Exception caught when handling data.", e);
            messageToDeadLetterExchange(jsonBody, e.getMessage());
        }

        latch.countDown();
    }

    @PreDestroy
    public void shutdown() {
        db.close();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
