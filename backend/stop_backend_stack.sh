#!/bin/bash

# Define environment variables
export QUEUE_USERNAME=root
export QUEUE_PASSWORD=root
export QUEUE_NAME=source

echo "Shutting down containers!"

sudo docker-compose -f docker-compose-backend.yml down