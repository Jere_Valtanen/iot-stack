Created by Jere-Joonas Valtanen


This is an IoT stack that collects worldwide air pollution data from the openaq API https://api.openaq.org

Data is moved through a rabbitMQ queue and then consumed into an influxDB database.

Grafana is used for displaying the data